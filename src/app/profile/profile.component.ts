import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loader = false;
  starRating = 0;
  emailChange='';
  emailVerify='';
  newEmail='';
  emailCode='';
  changeProfileImage='';
  UserData:any;
  message='';
  error_message='';
  message_image_license='';
  error_message_image_license='';
  message_image='';
  error_message_image='';
  message_email='';
  error_message_email='';
  message_license='';
  error_message_license='';
  message_account='';
  error_message_account='';
  ProfileForm: FormGroup;
  accountForm: FormGroup;
  licenseForm: FormGroup;
  emailForm: FormGroup;
  imageForm: FormGroup;
  imageSrc: string;
  imageSrcFront: string;
  imageSrcBack: string;
  submitted = false;
  dob='';
  addressList:any;
  keyword = 'address';
  selectedProfileFile = null;
  selectedLicenceFrontFile = null;
  selectedLicenceBackFile = null;
  isDrive: '';
  user_driving_license_front='';
  user_driving_license_back='';
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) { }

  get f(){
    return this.imageForm.controls;
  }

  toggleShow(val) {
    this.isDrive = val;
  }

  ngOnInit(): void {
    this.getProfileDetails();

    this.ProfileForm = this.formBuilder.group({
      fName: ['', Validators.required],
      lName: ['', Validators.required],
      mobile: ['', Validators.required],
      postcode: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      address_line_1: ['', Validators.required],
      address_line_2: [''],
      address_line_3: [''],
      address_line_4: [''],
      mobile_alt: [''],
      waist_size:  [''],
      trouser_length:  [''],
      top_size:[''],
    });

    this.accountForm = this.formBuilder.group({      
      nin:[''],
      bank_name:[''],
      account_name:[''],
      sort_code:[''],
      account_number:[''],
      payment_method:['']     
    });

    this.licenseForm = this.formBuilder.group({      
      drive_yes:[''],
      //license_back:[''],
      //license_front:[''],
      license_point_yes:['']        
    });

    this.emailForm = this.formBuilder.group({
      newEmail: ['', [Validators.required, Validators.email]],      
    });

    this.imageForm = this.formBuilder.group({      
      file:['', [Validators.required]],
      fileSource:['', [Validators.required]],                
    });
  }

  getYears() {
    const yearsList = [];
    const startYear=1920;
    const currentYear = new Date().getFullYear();
    for (let index = 0; index <= currentYear - startYear; index++) {
      yearsList.push(startYear + index);
    }
    return {yearsList};
  }

  
  getProfileDetails(){
    this.loader = true;
    this.apiService.getProfileDetails().subscribe(data => {
      //debugger;
      console.log(data);
      if (data.status === true) {
        this.UserData= data.data; 
        this.starRating= data.data.user_star_rating; 
        var Userdob = data.data.user_dob.split('-');
        this.user_driving_license_front= data.data.user_driving_license_front;
        this.user_driving_license_back= data.data.user_driving_license_back;
        window.localStorage.setItem('user_image', data.data.user_profile_image);
        this.ProfileForm.setValue({
          fName: data.data.user_first_name, 
          lName: data.data.user_last_name,
          mobile: (data.data.user_mob!=null)?data.data.user_mob:'',
          mobile_alt: (data.data.user_mob_alt!=null)?data.data.user_mob_alt:'',
          email: data.data.user_email,
          address_line_1: (data.data.user_add_1!=null)?data.data.user_add_1:'',
          address_line_2: (data.data.user_add_2!=null)?data.data.user_add_2:'',
          address_line_3: (data.data.user_add_3!=null)?data.data.user_add_3:'',
          address_line_4: (data.data.user_add_4!=null)?data.data.user_add_4:'',
          postcode: data.data.user_postcode,
          day: Userdob[2],
          month: Userdob[1],
          year: Userdob[0],
          waist_size:  (data.data.user_waist_size!=null)?data.data.user_waist_size:'',
          trouser_length:  (data.data.user_trouser_length!=null)?data.data.user_trouser_length:'',
          top_size: (data.data.user_top_size!=null)?data.data.user_top_size:''
        });
        this.accountForm.setValue({
          nin:(data.data.user_nin!=null)?data.data.user_nin:'',
          bank_name:(data.data.user_bank!=null)?data.data.user_bank:'',
          account_name:(data.data.user_account_name!=null)?data.data.user_account_name:'',
          sort_code:(data.data.user_sort_code!=null)?data.data.user_sort_code:'',
          account_number:(data.data.user_account_number!=null)?data.data.user_account_number:'',
          payment_method:data.data.user_payment_method,
        }); 
        this.licenseForm.setValue({
          drive_yes:data.data.user_driving_license,
          //license_back:data.data.user_driving_license_back,
          //license_front:data.data.user_driving_license_front,
          license_point_yes:data.data.user_driving_license_points          
        }); 
         window.localStorage.setItem('user_name', data.data.user_first_name+' '+data.data.user_last_name);
      } else {
        this.message=data.message;
        //alert(data.message);
      }
      this.loader = false;
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
        this.loader = false;
      }
    });
  }

  
  get registerFormControl() {
    return this.ProfileForm.controls;
  }
  
  onSubmit() {
    this.submitted = true;    
    if (this.ProfileForm.valid) { 
      this.dob=this.ProfileForm.controls.year.value+'-'+this.ProfileForm.controls.month.value+'-'+this.ProfileForm.controls.day.value;
      
      const profilePayload = {
        firstname: this.ProfileForm.controls.fName.value,
        lastname: this.ProfileForm.controls.lName.value,
        //email: this.ProfileForm.controls.email.value,
        postcode: this.ProfileForm.controls.postcode.value,
        dob: this.dob,         
        mob_alt: this.ProfileForm.controls.mobile_alt.value,        
        mob: this.ProfileForm.controls.mobile.value,        
        add_1: (this.ProfileForm.controls.address_line_1.value!=null)?this.ProfileForm.controls.address_line_1.value:'',   
        add_2: (this.ProfileForm.controls.address_line_2.value!=null)?this.ProfileForm.controls.address_line_2.value:'',   
        add_3: (this.ProfileForm.controls.address_line_3.value!=null)?this.ProfileForm.controls.address_line_3.value:'',   
        add_4: (this.ProfileForm.controls.address_line_4.value!=null)?this.ProfileForm.controls.address_line_4.value:'', 
        waist_size: this.ProfileForm.controls.waist_size.value, 
        trouser_length: this.ProfileForm.controls.trouser_length.value, 
        top_size: this.ProfileForm.controls.top_size.value, 

      }
      //console.log(profilePayload); return false;
      this.apiService.updateUser(profilePayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          if(data.data.failure_notes!=null){
            //alert(data.data.failure_notes);
            this.error_message= data.data.failure_notes;
          }else{
            this.message=data.message;
            //alert(data.message);
            //this.ngOnInit();
            this.getProfileDetails();
          }      
          
        }else {
          this.error_message=data.message;
         // alert(data.message);
        }
        
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });
      this.autoHideMessage();
    }
  }


  onSubmitAccount() {
    this.submitted = true;
    if (this.accountForm.valid) { 
      const profilePayload = {
        nin: this.accountForm.controls.nin.value,
        bank: this.accountForm.controls.bank_name.value,
        account_name: this.accountForm.controls.account_name.value,   
        sort_code: this.accountForm.controls.sort_code.value, 
        account_number: this.accountForm.controls.account_number.value, 
        payment_method: this.accountForm.controls.payment_method.value,
      }
      
      //console.log(profilePayload); return false;
      this.apiService.updateUser(profilePayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          if(data.data.failure_notes!=null){
            //alert(data.data.failure_notes);
            this.error_message_account= data.data.failure_notes;
          }else{
            this.message_account=data.message;
            //alert(data.message);
            //this.ngOnInit();
            this.getProfileDetails();
          }      
          
        }else {
          this.error_message_account=data.message;
         // alert(data.message);
        }
        
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message_account=error.error.message;
        }
      });
      this.autoHideMessage();
    }
  }

  onSubmitlicense() {
    this.submitted = true;
    if (this.licenseForm.valid) { 
      const profilePayload = {
        driving_license: this.licenseForm.controls.drive_yes.value,
        driving_license_points: this.licenseForm.controls.license_point_yes.value                 
      }
      
      //console.log(profilePayload); return false;
      this.apiService.updateUser(profilePayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          if(data.data.failure_notes!=null){
            //alert(data.data.failure_notes);
            this.error_message_license= data.data.failure_notes;
          }else{
            this.message_license=data.message;
            //alert(data.message);
            //this.ngOnInit();
            this.getProfileDetails();
          }      
          
        }else {
          this.error_message_license=data.message;
         // alert(data.message);
        }
        
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message_license=error.error.message;
        }
      });
      this.autoHideMessage();
    }
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
      this.message_image='';                      
      this.error_message_image='';
      this.message_email='';                      
      this.error_message_email='';
      this.message_license='';                      
      this.error_message_license='';
      this.message_account='';                      
      this.error_message_account='';
      this.error_message_image_license='';
      this.message_image_license='';
    }, 5000);
  }


  onChangeSearch(str: string){    
    //if(str.length>=3){
      const addressPayload = {
        search: str,
      }
      this.apiService.getAddressList(addressPayload).subscribe(data => {
        //debugger;
        //console.log(data);
        if(data.status === true) {
           this.addressList=data.data;           
           //console.log(this.addressList); 
        }else {
          this.error_message=data.message;         
        }
        
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });
    //}
  }

  selectEvent(item) {
    this.addressList=[];
    const addressPayload = {
      id: item.id,
    }
    this.apiService.getAddressView(addressPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if(data.status === true) {
         this.addressList=data.data;  
         this.ProfileForm.get('address_line_1').setValue(data.data.add_1);         
         this.ProfileForm.get('address_line_2').setValue(data.data.add_2);         
         this.ProfileForm.get('address_line_3').setValue(data.data.add_3);         
         this.ProfileForm.get('address_line_4').setValue(data.data.add_4);         
         this.ProfileForm.get('postcode').setValue(data.data.postcode);         
         //console.log(this.addressList); 
         this.addressList=[];
      }     
    });
  }
 
  
  
  onFocused(e){
    this.addressList=[];
    // here we can write our code for doing something when input is focused
  }

  changeEmail(val){
    this.emailChange=val;
    this.emailForm.get('newEmail').setValue('');    
  }

  changeEmailVerify(val){
    this.emailVerify='';
    this.emailChange=val;
    this.emailCode='';
    this.emailForm.get('newEmail').setValue('');    
  }
  
  sendEmailCode(){
    const emailPayload = {
      email: this.emailForm.controls.newEmail.value, 
      step: 1, 
    }
    // console.log(emailPayload); return false;
    this.apiService.editEmail(emailPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if(data.status === true) {
        this.emailVerify='1';
        this.emailChange='';
       this.message_email=data.message;           
      }else {
        this.error_message_email=data.message;
       // alert(data.message);
      }      
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message_email=error.error.message;
      }
    });
    this.autoHideMessage();
  }

  get emailFormControl() {
    return this.emailForm.controls;
  }

  verifyEmailCode(code){
    console.log(code);
    const emailPayload = {
      code: code, 
      step: 2, 
    }
    // console.log(profilePayload); return false;
    this.apiService.editEmail(emailPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if(data.status === true) {
        this.emailVerify='';
       this.message_email=data.message;
       this.ngOnInit();           
      }else {
        this.error_message_email=data.message;
       // alert(data.message);
      }
      
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message_email=error.error.message;
      }
    });
    this.autoHideMessage();
  }


  changeImage(val){
    this.changeProfileImage=val;
    this.selectedProfileFile=null;
  }


  onFileChange(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.selectedProfileFile = event.target.files[0];
      //console.log(this.selectedProfileFile);
      reader.readAsDataURL(file);
    
      reader.onload = () => {
   
        this.imageSrc = reader.result as string;
     
        this.imageForm.patchValue({
          fileSource: reader.result
        });
   
      };
      const imagePayload = {
        file_upload: this.selectedProfileFile,
        type:'profile',
        format:'json'
      }
      //console.log(imagePayload);
      this.apiService.uploadProfileImage(imagePayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          this.message_image=data.message;  
          this.ngOnInit();
          this.changeProfileImage='';
          this.selectedProfileFile=null;
        }else {
          this.error_message_image=data.message;      
        }      
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message_image=error.error.message;
        }
      });
      this.autoHideMessage();
    }
  }
   
  uploadImage(){
    //console.log(this.imageForm.value);
    const imagePayload = {
      file_upload: this.selectedProfileFile,
      type:'profile',
      format:'json'
    }
    //console.log(imagePayload);
    this.apiService.uploadProfileImage(imagePayload).subscribe(data => {
      //debugger;
      console.log(data);
      if(data.status === true) {
        this.message_image=data.message;  
        this.ngOnInit();
        this.changeProfileImage='';
        this.selectedProfileFile=null;
      }else {
        this.error_message_image=data.message;      
      }      
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message_image=error.error.message;
      }
    });
    this.autoHideMessage();
  }

  onFileChangeLicence(event, type) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.selectedLicenceFrontFile = event.target.files[0];
      //console.log(this.selectedLicenceFrontFile);selectedLicenceBackFile
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        if(type=='driving_license_front'){
          this.imageSrcFront = reader.result as string;
        }else{
          this.imageSrcBack = reader.result as string;
        }
        
     
        this.licenseForm.patchValue({
          fileSource: reader.result
        });
   
      };
      
      const imagePayload = {
        file_upload: this.selectedLicenceFrontFile,
        type:type,
        format:'json'
      }
      // console.log(imagePayload); return false;
      this.apiService.uploadProfileImage(imagePayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          this.message_image_license=data.message;  
          //this.ngOnInit();
          this.selectedLicenceFrontFile=null;
          this.selectedLicenceBackFile=null;
        }else {
          this.error_message_image_license=data.message;      
        }      
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message_image_license=error.error.message;
        }
      });
      this.autoHideMessage();

    }
  }
}
