import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from "../service/api.service";
import { NgbModal, ModalDismissReasons, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-favourite-jobs',
  templateUrl: './favourite-jobs.component.html',
  styleUrls: ['./favourite-jobs.component.scss']
})
export class FavouriteJobsComponent implements OnInit {
  @ViewChild("filterOrderBy", ) private filterOrderBy: ElementRef;
  // @ViewChild("filterByRadius", ) private filterByRadius: ElementRef;
  @ViewChild("filterByExpired", ) private filterByExpired: ElementRef;
  @ViewChild("filterByStaffed", ) private filterByStaffed: ElementRef;
  @ViewChild("filterBySort", ) private filterBySort: ElementRef;
  // @ViewChild("filterByDate", ) private filterByDate: ElementRef;
  @ViewChild("filterByPage", ) private filterByPage: ElementRef;
  loader = false;
  isAgree: boolean = false;
  isFilter:boolean=false;
  closeResult: string;
  row_id=''; 
  message = '';
  error_message='';
  message_apply='';
  error_message_apply='';
  totalJobs=0;
  total= null;
  next= null;
  prev= null;
  next_url= null;
  prev_url= null;
  this_page= null;
  total_pages= null;

  date='';
  hide_staffed=0;
  show_expired=0;
  page=1;
  search='';
  after='';
  limit=50;
  order='DESC';
  order_by='job_id';
  radius:number;
  constructor(private modalService: NgbModal, private apiService: ApiService) { }

  jobs: any;
  jobDetails: any;
  model: NgbDateStruct;

  ngOnInit(): void {
    this.loader = true;
    this.getFavouriteJobList();
    this.jobDetails=[];
  }

  getRequestParams(search, page, limit, after, order_by, order, radius, hide_staffed, show_expired, date) {
    
    let params = {};
    if (search) {
      params[`search`] = search;
    }
    if (page) {
      params[`page`] = page;
    }
    if (limit) {
      params[`limit`] = limit;
    }
    if (after) {
      params[`after`] = after;
    }
    if (order_by) {
      params[`order_by`] = order_by;
    }
    if (order) {
      params[`order`] = order;
    }    
    if (radius) {
      params[`radius`] = radius;
    }
    if (hide_staffed) {
      params[`hide_staffed`] = hide_staffed;
    }
    if (show_expired) {
      params[`show_expired`] = show_expired;
    }
    if (date) {
      params[`date`] = date;
    }

    return params;
  }

  getFavouriteJobList() { 
    this.loader = true;   
    const params = this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.radius, this.hide_staffed, this.show_expired, this.date);
    console.log(params);
    this.apiService.getFavouriteJobList(params).subscribe(data => {
      if (data.status === true) {
        if( data.data!==null){        
          this.jobs = data.data;                   
        }else{
          this.jobs =[];          
        }
        
        this.total_pages = data.metadata.total_pages;
        this.this_page= data.metadata.this_page;
        this.total= data.metadata.total;
        this.next= data.metadata.next;
        this.prev= data.metadata.prev;
        this.next_url= data.metadata.next_url;
        this.prev_url= data.metadata.prev_url;
        this.limit= data.metadata.limit;

        this.filterOrderBy.nativeElement.value = this.order_by;
        //this.filterByRadius.nativeElement.value = this.radius;
        this.filterBySort.nativeElement.value = this.order;
        //this.filterByDate.nativeElement.value = this.date;
        this.filterByPage.nativeElement.value = this.limit;
        this.filterByExpired.nativeElement.value = this.show_expired;
        this.filterByStaffed.nativeElement.value = this.hide_staffed;


        console.log(data);
      } else {
        this.jobs = [];        
      }
      this.loader = false;
    },
    error => {
      console.log(error);
      this.loader = false;      
    });
    
  }

  handlePageChange(event) {
    this.page = event;
    this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.radius, this.hide_staffed, this.show_expired, this.date);
    this.getFavouriteJobList();
  }


  onFilterChangeOrderBy(val){
    this.isFilter=true;
    this.jobs=[];
    this.order_by=val;
    console.log(val);
    this.getFavouriteJobList();
  }

  onFilterChangeRadius(val){
    this.isFilter=true;
    this.jobs=[];
    this.radius=val;
    console.log(val);
    this.getFavouriteJobList();
  }
  onFilterChangeExpired(val){
    this.isFilter=true;
    this.jobs=[];
    if(val==1){
      this.show_expired=0;
    }else{
      this.show_expired=1;
    }
    //this.show_expired=val;
    console.log(val);
    this.getFavouriteJobList();
  }
  onFilterChangeStaffed(val){
    this.isFilter=true;
    this.jobs=[];
    if(val==1){
      this.hide_staffed=0;
    }else{
      this.hide_staffed=1;
    }
    //this.hide_staffed=val;
    console.log(val);
    this.getFavouriteJobList();
  }
  onFilterChangeSortBy(val){
    this.isFilter=true;
    this.jobs=[];
    if(val=='ASC'){
      this.order="DESC";
    }else{
      this.order="ASC";
    }
    //this.order=val;
    console.log(val);
    this.getFavouriteJobList();
  }
  onFilterChangeDate(val){
    this.isFilter=true;
    this.jobs=[];
    this.date=val;
    console.log(val);
    this.getFavouriteJobList();
  }
  onFilterChangeLimit(val){
    this.isFilter=true;
    this.jobs=[];
    this.limit=val;
    this.page=1;
    console.log(val);
    this.getFavouriteJobList();
  }

  clearFilters(){
    this.isFilter=false;
    this.jobs=[];
    this.date='';
    this.hide_staffed=0;
    this.show_expired=0;
    this.page=1;
    this.search='';
    this.after='';
    this.limit=50;
    this.order='ASC';
    this.order_by='job_id';
    this.radius=100;
    this.getFavouriteJobList();
  }

  open(content, id) {
    this.isAgree = false;
    this.message_apply='';     
    this.error_message_apply=''; 
    this.apiService.viewJob(id).subscribe(data => {
      //debugger;      
      if (data.status === true) {
        this.jobDetails = data.data;
      } else {
        this.jobDetails = [];
      }

      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });

    });

  }
  
  favouriteJobAdd(id) {  
    const favJobPayload = {
      job_id: id,      
    }      
    this.apiService.favouriteJobAdd(favJobPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if (data.status === true) {
        //alert(data.message);
        this.message=data.message;
      } else {
        //alert(data.message);
        this.error_message=data.message;
      }
      this.ngOnInit();
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });  
    this.autoHideMessage();     
  }


  favouriteJobAddDetails(id) {  
    const favJobPayload = {
      job_id: id,      
    }    
    this.apiService.favouriteJobAdd(favJobPayload).subscribe(data => {
      //debugger;
      console.log(data);
      
      if (data.status === true) {
        //alert(data.message);
        this.apiService.viewJob(id).subscribe(res => {
          //debugger;     
          console.log(res.data); 
          if (res.status === true) {
            this.jobDetails = res.data;        
          }           
        });
      } else {
        //alert(data.message);
        this.error_message=data.message;

      }
      this.ngOnInit();
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });

    this.autoHideMessage(); 
          
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  applyJob(id) {
    const jobPayload = {
      job_id: id,
    }
    this.row_id= id;
    this.apiService.applyJob(jobPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if (data.status === true) {
        this.message_apply = data.message;
        this.apiService.viewJob(id).subscribe(data => {
          //debugger;     
          if (data.status === true) {
            this.jobDetails = data.data;        
          } else {
            this.jobDetails = [];
          }    
        });
      } else {
        this.error_message_apply = data.message;        
      }
    },
      error => {
        if (error.status == 400) {
          console.log(error.error.message);
          this.error_message_apply = error.error.message;
        }
      });
      
      setTimeout(()=>{ 
        this.message_apply='';                      
        this.error_message_apply='';
      }, 5000);
  }


  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }

  
  calculateDiff(data) {
    let date = new Date(data);
    let currentDate = new Date();

    let days = Math.floor((currentDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24);
    return days;
  }


  toggleShow() {
    this.isAgree = !this.isAgree;
  }

}
