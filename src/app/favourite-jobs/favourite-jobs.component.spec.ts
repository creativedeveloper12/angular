import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteJobsComponent } from './favourite-jobs.component';

describe('FavouriteJobsComponent', () => {
  let component: FavouriteJobsComponent;
  let fixture: ComponentFixture<FavouriteJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
