import { Component, OnInit } from '@angular/core';
import {ApiService} from "../service/api.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  loader = false;
  UserData:any;
  message='';
  error_message='';
  settingForm: FormGroup;
  submitted = false; 
  constructor(private formBuilder: FormBuilder, private apiService: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.loader = true;
    this.apiService.getProfileDetails().subscribe(data => {
      //debugger;
      console.log(data);
      if (data.status === true) {
        this.UserData= data.data;   
        this.settingForm.setValue({
          email_notification: data.data.user_email_notify, 
          sms_notification: data.data.user_sms_notify,
          job_radius: (data.data.user_notify_jobs_radius)*2,
          job_notification: data.data.user_notify_jobs,
          message_notification: data.data.user_notify_messages,
          
        });
        
      } else {
        this.message=data.message;
        //alert(data.message);
      }
      this.loader = false;
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
        this.loader = false;
      }
    }); 

    this.settingForm = this.formBuilder.group({
      email_notification: [''],
      sms_notification: [''],
      job_radius: [''],
      job_notification: [''],
      message_notification: [''],     
    });
  }



  onSubmitSetting() {
    this.submitted = true;
    if (this.settingForm.valid) { 
      const profilePayload = {
        email_notify: this.settingForm.controls.email_notification.value,
        sms_notify: this.settingForm.controls.sms_notification.value,
        notify_jobs_radius: Math.round((this.settingForm.controls.job_radius.value)/2),
        notify_jobs: this.settingForm.controls.job_notification.value,
        notify_messages: this.settingForm.controls.message_notification.value,
      }
      
      console.log(profilePayload); 
      // return false;
      this.apiService.updateUser(profilePayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          if(data.data.failure_notes!=null){
            this.error_message= data.data.failure_notes;
          }else{
            this.message=data.message;            
          }      
          
        }else {
          this.error_message=data.message;         
        }
        
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });
      this.autoHideMessage();
    }
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';      
    }, 5000);
  }


  onLogout(){
    const logoutPayload = {      
      logout_all: 1, 
    }
    this.apiService.logout(logoutPayload).subscribe(data => {
      //debugger;
      if(data.status === true) {
        //localStorage.clear();
        localStorage.removeItem('token');
        localStorage.removeItem('user_name');
        this.message=data.message;
        this.router.navigate(['login']);
      }else {
        this.error_message=data.message;
        alert(data.message);
      }
    },
    error => {      
      if(error.status==400){
        this.error_message=error.error.message;
      }
    });                  
  }

}
