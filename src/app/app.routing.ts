import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from './register/register.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {AddUserComponent} from "./user/add-user/add-user.component";
import {ListUserComponent} from "./user/list-user/list-user.component";
import { EditUserComponent } from "./user/edit-user/edit-user.component";
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { MessagesComponent } from './messages/messages.component';
import { SettingsComponent } from './settings/settings.component';
import { FavouriteJobsComponent } from './favourite-jobs/favourite-jobs.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { JobOfferComponent } from './job-offer/job-offer.component';
import { YourReferralsComponent } from './your-referrals/your-referrals.component';
import { YourJobsComponent } from './your-jobs/your-jobs.component';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import { RegisterFBComponent } from './register-fb/register-fb.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'register-fb', component: RegisterFBComponent },
  { path: 'register/:referral', component: RegisterComponent },
  { path: 'email-verify', component: EmailVerifyComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password', component: ResetPasswordComponent },
  { path: 'add-user', component: AddUserComponent },
  { path: 'list-user', component: ListUserComponent },
  { path: 'edit-user', component: EditUserComponent },
  { path: 'home', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'messages', component: MessagesComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'favourite-jobs', component: FavouriteJobsComponent },
  { path: 'message-details/:id', component: MessageDetailsComponent },
  { path: 'job-offer', component: JobOfferComponent },
  { path: 'your-referrals', component: YourReferralsComponent },
  { path: 'your-jobs', component: YourJobsComponent },
  { path: 'calendar-view', component: CalendarViewComponent },
  { path : '', component : LoginComponent }
];

export const routing = RouterModule.forRoot(routes);