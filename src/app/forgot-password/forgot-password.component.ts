import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  message='';
  error_message='';
  userEmail='';
  invalidLogin: boolean = false;
  forgotPasswordForm: FormGroup;  
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) { }


  onSubmit() {
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    const passwordPayload = {
      email: this.forgotPasswordForm.controls.email.value,
      step: '1'      
    }
    //console.log(passwordPayload);   return false;  
    this.apiService.forgotPassword(passwordPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if(data.status === true) {        
        //alert(this.message=data.message);
        const emaildata = {
          email: this.forgotPasswordForm.controls.email.value
        };
        this.router.navigate(['reset-password'] , { state: { email: this.forgotPasswordForm.controls.email.value, message: data.message} });
      }else {
        this.invalidLogin = true;
        alert(data.message);
      }
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });

    this.autoHideMessage();
  }


  ngOnInit(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]        
    });
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }
}
