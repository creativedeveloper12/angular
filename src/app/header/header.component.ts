import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  Favjobs: any;
  UserData:any;
  user_name:string;
  user_image:string;
  closeResult: string;
  message='';
  error_message='';
  logout_all=0;
  locale:any;
  constructor(private modalService: NgbModal,private router: Router, private apiService: ApiService, public translate: TranslateService) {    
   }

  ngOnInit(): void {
    this.user_name = localStorage.getItem('user_name');    
    this.locale = window.localStorage.getItem('locale');
    this.apiService.getProfileDetails().subscribe(userData => {
      //debugger;
      if (userData.status === true) {        
        window.localStorage.setItem('user_image', userData.data.user_profile_image); 
        this.user_image = localStorage.getItem('user_image');     
      }
    })
  }

  getUserName(){
    return this.user_name = localStorage.getItem('user_name');    
  }

  getUserImage(){
    return this.user_image = localStorage.getItem('user_image');
  }
  getLocale() {
    return this.locale;
  }

  localeSwitcher(locale){
    window.localStorage.setItem('locale', locale);
    this.locale = locale;
    this.translate.use(locale);
  }

  onLogout(){
    const logoutPayload = {      
      logout_all: this.logout_all, 
    }
    this.apiService.logout(logoutPayload).subscribe(data => {
      //debugger;
      if(data.status === true) {
        //localStorage.clear();
        localStorage.removeItem('token');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_image');
        this.message=data.message;
        //alert(data.message);
        this.router.navigate(['login']);
      }else {
        this.error_message=data.message;
        alert(data.message);
      }
    },
    error => {      
      if(error.status==400){
        this.error_message=error.error.message;
      }
    });

    this.autoHideMessage();               
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }
  
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'fullscreen', windowClass: 'main-menu-modal' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}
