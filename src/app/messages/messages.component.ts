import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from "../service/api.service";
import {FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  @ViewChild("filterBySort", ) private filterBySort: ElementRef;
  @ViewChild("filterByPage", ) private filterByPage: ElementRef;
  loader = false;
  isFilter:boolean=false;
  messageList: any;
  pageList: any;
  closeResult: string;
  message='';
  error_message='';
  invalidMessage: boolean = false;
  
  total= null;
  next= null;
  prev= null;
  next_url= null;
  prev_url= null;
  this_page= null;
  total_pages= null;

  page=1;
  search='';
  after='';
  limit=10;
  order='DESC';
  order_by='thread_updated_time';
  messageForm: FormGroup;
  searchForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private modalService: NgbModal, private apiService: ApiService) { }

  ngOnInit(): void {
    this.getMessageList();  
    
    this.messageForm = this.formBuilder.group({
      newMessage: ['', Validators.required],      
    });
    this.searchForm = this.formBuilder.group({
      searchText:this.search,      
    });
  }

  getRequestParams(search, page, limit, after, order_by, order) {
    // tslint:disable-next-line:prefer-const
    let params = {};
    if (search) {
      params[`search`] = search;
    }
    if (page) {
      params[`page`] = page;
    }
    if (limit) {
      params[`limit`] = limit;
    }
    if (after) {
      params[`after`] = after;
    }
    if (order_by) {
      params[`order_by`] = order_by;
    }
    if (order) {
      params[`order`] = order;
    }

    return params;
  }

  getMessageList() {
    this.loader = true;
    const params = this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order);
    console.log(params);
    this.apiService.getThreadList(params).subscribe(data => {
      if (data.status === true) {
        if( data.data!==null){        
          this.messageList = data.data;
          
        }else{
          this.messageList =[];
         
        }
        this.total_pages = data.metadata.total_pages;
        this.this_page= data.metadata.this_page;
        this.total= data.metadata.total;
        this.next= data.metadata.next;
        this.prev= data.metadata.prev;
        this.next_url= data.metadata.next_url;
        this.prev_url= data.metadata.prev_url;
        this.limit= data.metadata.limit;
        this.filterBySort.nativeElement.value = this.order;
        this.filterByPage.nativeElement.value = this.limit;
        console.log(data);
        this.loader = false;
      } else {
        this.messageList = [];
        this.loader = false;
      }
    },
    error => {
      console.log(error);
      this.loader = false;
    });
  }

  counter(i: number) {
      return new Array(i);
  }

  handlePageChange(event) {
    this.page = event;
    this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order);
    this.getMessageList();
  }

  onChangeEvent(searchValue: any){
    if(searchValue==''){
      this.page=1;
      this.search=searchValue;
      this.ngOnInit();
    }
  }

  searchMessage() {
    this.page=1;
    this.search=this.searchForm.controls.searchText.value;
    this.ngOnInit();
  }

  onFilterChangeLimit(val){
    this.isFilter=true;
    this.messageList = [];
    this.limit=val;
    this.page=1;
    console.log(val);
    this.getMessageList();
  }
  onFilterChangeSortBy(val){
    this.isFilter=true;
    this.messageList = [];
    if(val=='ASC'){
      this.order="DESC";
    }else{
      this.order="ASC";
    }
    //this.order=val;
    console.log(val);
    this.getMessageList();
  }
  open(content) {
    this.error_message='';
    this.message='';
    this.messageForm.get('newMessage').setValue('');
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'new-msg-modal' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  

  onSubmit(){
    if (this.messageForm.invalid) {
      return;
    }
    const messagePayload = {
      message: this.messageForm.controls.newMessage.value,  
      thread_id: 0,  
    }    
    this.apiService.sendMessage(messagePayload).subscribe(data => {
      //debugger;
     
      if(data.status === true) {        
        this.message=data.message;       
        this.ngOnInit();
        this.modalService.dismissAll();
      }else {
        this.error_message=data.message;
        
      }
    },error => {     
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });
    this.autoHideMessage();
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }
}
