import { Component, OnInit, HostListener } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider } from "angularx-social-login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  deferredPrompt: any;
  showButton = false;
  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(e: { preventDefault: () => void; }) {
    console.log(e);
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    this.deferredPrompt = e;
    this.showButton = true;
  }

  message='';  
  loginForm: FormGroup;
  invalidLogin: boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService, private authService: SocialAuthService) {
    
  }

  addToHomeScreen() {
    // hide our user interface that shows our A2HS button
    this.showButton = false;
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice
      .then((choiceResult: { outcome: string; }) => {
        if (choiceResult.outcome === 'accepted') {
          this.showButton = false;
          console.log('User accepted the A2HS prompt');
        } else {
          this.showButton = true;
          console.log('User dismissed the A2HS prompt');
        }
        this.deferredPrompt = null;
      });
  }

  isAppInstall(){
    // console.log("ok");
    // window.addEventListener('beforeinstallprompt', function (event) { 
    //   this.deferredPrompt = event; 
    //   console.log(event); 
    // });       
      return this.showButton; 
  }

  
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID, {cookie:true, xfbml:true, version:'v11.0'})
    .then(res => { 
      // console.log(res);      
      if(res.authToken!=''){
        const loginPayload = {
          token: res.authToken,        
        }
       // console.log(loginPayload);
        this.apiService.facebookLogin(loginPayload).subscribe(data => {        
          //console.log(data);           
          if(data.status === true) {
            if(data.metadata.twofactorrequired===false && data.metadata.registrationrequired===false){
              window.localStorage.setItem('token', data.data.token);
              this.router.navigate(['home']);
              this.message=data.message;             
              window.localStorage.setItem('token', data.data.token);
              window.localStorage.setItem('user_name', data.data.first_name+' '+data.data.last_name);
              
            }else if(data.metadata.twofactorrequired===true && data.metadata.registrationrequired===false){
              this.message=data.message;
              this.router.navigate(['emailverify'], { state: { email: data.data.email, twofactortoken:data.data.twofactortoken, token:res.authToken } });
            }else if(data.metadata.twofactorrequired===false && data.metadata.registrationrequired===true){
              
              this.router.navigate(['register-fb'], { state: { token:res.authToken, first_name: data.data.registration_suggestions.first_name, last_name: data.data.registration_suggestions.last_name, email: data.data.registration_suggestions.email, refer_code: data.data.registration_suggestions.refer_code, postcode: data.data.registration_suggestions.postcode } });
            }
            
    
          }else {
            this.invalidLogin = true;
            this.message=data.message;
          }
        },error => {      
          if(error.status==401){
            this.message=error.error.message;
            console.log(error);
          }
        });
      }
      
    });    
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const loginPayload = {
      email: this.loginForm.controls.username.value,
      password: this.loginForm.controls.password.value
    }
    this.apiService.login(loginPayload).subscribe(data => {
      //debugger;
     
      if(data.status === true) {
        window.localStorage.setItem('token', data.data.token);
        this.router.navigate(['home']);
        this.message=data.message;
        this.apiService.getProfileDetails().subscribe(userData => {
          //debugger;
          if (userData.status === true) {
            
            window.localStorage.setItem('token', window.localStorage.getItem('token'));
            window.localStorage.setItem('user_name', userData.data.user_first_name+' '+userData.data.user_last_name);     
            window.localStorage.setItem('user_image', userData.data.user_profile_image);     
          } else {
            this.message=userData.message;            
          }
        })

      }else {
        this.invalidLogin = true;
        this.message=data.message;
        //alert(data.message);
      }
    },error => {      
      if(error.status==401){
        this.message=error.error.message;
      }
    });
    this.autoHideMessage();
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message=''; 
    }, 5000);
  }
  ngOnInit() {    
    if (window.localStorage.getItem('token')) {      
        this.router.navigate(['home']);             
    }else{
       window.localStorage.removeItem('token');
    }
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
   
  }



}