import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from "../service/api.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-job-offer',
  templateUrl: './job-offer.component.html',
  styleUrls: ['./job-offer.component.scss']
})
export class JobOfferComponent implements OnInit {
  @ViewChild("filterBySort", ) private filterBySort: ElementRef;
  @ViewChild("filterByPage", ) private filterByPage: ElementRef;
  loader=false;
  row_id= '';
  closeResult: string;
  message = '';
  error_message='';
  isFilter:boolean=false;
  jobs: any;
  jobDetails: any;
  total= null;
  next= null;
  prev= null;
  next_url= null;
  prev_url= null;
  this_page= null;
  total_pages= null;

  date='';
  page=1;
  search='';
  after='';
  limit=50;
  order='DESC';
  order_by='job_id';
  radius:number;
  offered_only=1;
  constructor(private apiService: ApiService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.loader = true;
    this.getJobList();
    this.jobDetails=[];
  }

  getRequestParams(search, page, limit, after, order_by, order, radius, date, offered_only) {
    
    let params = {};
    if (search) {
      params[`search`] = search;
    }
    if (page) {
      params[`page`] = page;
    }
    if (limit) {
      params[`limit`] = limit;
    }
    if (after) {
      params[`after`] = after;
    }
    if (order_by) {
      params[`order_by`] = order_by;
    }
    if (order) {
      params[`order`] = order;
    }    
    if (radius) {
      params[`radius`] = radius;
    }    
    if (date) {
      params[`date`] = date;
    }
    if (offered_only) {
      params[`offered_only`] = offered_only;
    }

    return params;
  }


  getJobList() {  //offered job list
    this.loader = true;   
    const params = this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.radius, this.date, this.offered_only);
    console.log(params);
    this.apiService.getAppliedJobList(params).subscribe(data => {
      if (data.status === true) {
        if( data.data!==null){        
          this.jobs = data.data;               
        }else{
          this.jobs =[];          
        }
         
        this.total_pages = data.metadata.total_pages;
        this.this_page= data.metadata.this_page;
        this.total= data.metadata.total;
        this.next= data.metadata.next;
        this.prev= data.metadata.prev;
        this.next_url= data.metadata.next_url;
        this.prev_url= data.metadata.prev_url;
        this.limit= data.metadata.limit;
        this.filterBySort.nativeElement.value = this.order;
        this.filterByPage.nativeElement.value = this.limit;
        console.log(data);
      } else {
        this.jobs = [];       
      }
      this.loader = false;
    },
    error => {
      console.log(error);
      this.loader = false;
    });
    
  }


  handlePageChange(event) {
    this.page = event;
    this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.radius, this.date, this.offered_only);
    this.getJobList();
  }


  onFilterChangeOrderBy(val){
    this.isFilter=true;
    this.jobs=[];
    this.order_by=val;
    console.log(val);
    this.getJobList();
  }

  onFilterChangeRadius(val){
    this.isFilter=true;
    this.jobs=[];
    this.radius=val;
    console.log(val);
    this.getJobList();
  }   
  onFilterChangeSortBy(val){
    this.isFilter=true;
    this.jobs=[];
    if(val=='ASC'){
      this.order="DESC";
    }else{
      this.order="ASC";
    }
    //this.order=val;
    console.log(val);
    this.getJobList();
  }
  onFilterChangeDate(val){
    this.isFilter=true;
    this.jobs=[];
    this.date=val;
    console.log(val);
    this.getJobList();
  }
  onFilterChangeLimit(val){
    this.isFilter=true;
    this.jobs=[];
    this.limit=val;
    this.page=1;
    console.log(val);
    this.getJobList();
  }

  clearFilters(){
    this.isFilter=false;
    this.jobs=[];
    this.date='';
    this.page=1;
    this.search='';
    this.after='';
    this.limit=50;
    this.order='ASC';
    this.order_by='job_id';
    this.getJobList();
  }

  calculateDiff(data) {
    let date = new Date(data);
    let currentDate = new Date();

    let days = Math.floor((currentDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24);
    return days;
  }

  open(content, id) {      
    this.apiService.viewJob(id).subscribe(data => {
      //debugger;     
      console.log(data.data); 
      if (data.status === true) {
        this.jobDetails = data.data;        
      } else {
        this.jobDetails = [];
      }

      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });

    });
        
  }

  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  offerJobEdit(id, action) {  
    const JobOfferPayload = {
      job_id: id, 
      action:action     
    }
    this.row_id= id; 
    console.log(JobOfferPayload);      
    this.apiService.offerJobEdit(JobOfferPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if (data.status === true) {
        //alert(data.message);
        this.message=data.message;
        this.apiService.viewJob(id).subscribe(data => {
          //debugger;     
          if (data.status === true) {
            this.getJobList();
            this.jobDetails = data.data;        
          } else {
            this.jobDetails = [];
          }    
        });
      } else {
        //alert(data.message);
        this.error_message=data.message;
      }
      // this.ngOnInit();
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });  
    this.autoHideMessage();     
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }


  favouriteJobAddDetails(id) {  
    const favJobPayload = {
      job_id: id,      
    }    
    this.apiService.favouriteJobAdd(favJobPayload).subscribe(data => {
      //debugger;
      console.log(data);
      
      if (data.status === true) {
        //alert(data.message);
        this.apiService.viewJob(id).subscribe(res => {
          //debugger;     
          console.log(res.data); 
          if (res.status === true) {
            this.jobDetails = res.data;        
          }           
        });
      } else {
        //alert(data.message);
        this.error_message=data.message;

      }
      //this.ngOnInit();
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });

    this.autoHideMessage(); 
          
  }

}
