import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from "../model/user.model";
import { Observable } from "rxjs/index";
import { ApiResponse } from "../model/api.response";
import { General } from "./../general";

@Injectable()
export class ApiService {

  private general: General = new General();

  constructor(private http: HttpClient) { }
  baseUrl: string = 'https://api.wepaysameday.co.uk/v1/';

  login(loginPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'login', this.general.getFormData(loginPayload));
  }

  logout(logoutPayload): Observable<ApiResponse> {
    // let headers = new HttpHeaders()
    // .set('S-Token', window.localStorage.getItem('token'));, {'headers':headers}
    return this.http.post<ApiResponse>(this.baseUrl + 'logout', logoutPayload);
  }

  forgotPassword(passwordPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'login/reset', this.general.getFormData(passwordPayload));
  }

  getUsers(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'job/list');
  }

  getProfileDetails(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'user/view');
  }

  getJobs(jobPayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'job/list', {params: jobPayload});
  }

  getFavouriteJobList(jobPayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'job/favourite/list', {params: jobPayload});
  }

  viewJob(id): Observable<ApiResponse> {
    let params = new HttpParams();
    params = params.append('job_id', id);
    return this.http.get<ApiResponse>(this.baseUrl + 'job/view', { params: params });
  }

  applyJob(jobPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'job/apply/add', this.general.getFormData(jobPayload));
  }

  favouriteJobAdd(favJobPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'job/favourite/toggle', this.general.getFormData(favJobPayload));
  }
  getAppliedJobList(jobPayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'job/applied/list', { params: jobPayload });
  }

  offerJobEdit(JobOfferPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'job/offer/edit', this.general.getFormData(JobOfferPayload));
  }

  // getUserById(id: number): Observable<ApiResponse> {
  //   return this.http.get<ApiResponse>(this.baseUrl + id);
  // }

  createUser(registerPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'register', this.general.getFormData(registerPayload));
  }

  updateUser(profilePayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'user/edit', this.general.getFormData(profilePayload));
  }

  // deleteUser(id: number): Observable<ApiResponse> {
  //   return this.http.delete<ApiResponse>(this.baseUrl + id);
  // }

  getThreadList(messagePayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'message/thread/list', {params: messagePayload});
  }

  getMessageList(messagePayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'message/list', {params: messagePayload});

  }

  sendMessage(messagePayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl+ 'message/add', this.general.getFormData(messagePayload));
  }

  getReferralList(referralPayload): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'referral/list', {params: referralPayload});
  }

  getCalendarView(datePayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'calendar/view', {params: datePayload});
  }

  getAddressList(addressPayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'address/list', {params: addressPayload});
  }

  getAddressView(addressPayload): Observable<ApiResponse> {
    let params = new HttpParams();
    return this.http.get<ApiResponse>(this.baseUrl + 'address/view', {params: addressPayload});
  }

  editEmail(emailPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl+ 'user/email/edit', this.general.getFormData(emailPayload));
  }

  uploadProfileImage(imagePayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl+ 'image/upload', this.general.getFormData(imagePayload));
  }

  facebookLogin(loginPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl+ 'facebook/login', this.general.getFormData(loginPayload));
  }

  
}