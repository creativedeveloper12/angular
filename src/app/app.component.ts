import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'appBootstrap';
  closeResult: string;
  loggedIn: boolean; 
  isMessageDetailsPage:any; 
  constructor(private modalService: NgbModal, private router: Router, public translate: TranslateService, private titleService: Title, private metaTagService: Meta) {
    /**
     * Translator.
     */
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');
    let locale = 'en';
    console.log(window.localStorage.getItem('locale'));
    if( window.localStorage.getItem('locale') == null || window.localStorage.getItem('locale') == "" ){
      console.log(123)
      window.localStorage.setItem('locale', locale);
    }else{
      locale = window.localStorage.getItem('locale');
    }
    translate.use(locale);

    const browserLang = translate.getBrowserLang();
    console.log('lang:'+browserLang);

    var AllowPages = ['login', 'register', 'emailverify', 'forgot-password', 'reset-password', 'register-fb'];
    var current_segment = window.location.pathname;
    var newstr = current_segment.replace('/', '');
    const isMatch = current_segment.match('register/');
    
    
    if (!window.localStorage.getItem('token')) {
      if (!AllowPages.includes(newstr) && isMatch==null) {
        router.navigate(['login']);
      }else if(isMatch!=null && isMatch.length>0){
        router.navigate([current_segment]);
      }
    }
  }

  ngOnInit() {
    this.titleService.setTitle("We pay same day");  
    this.metaTagService.addTags([
      { name: 'keywords', content: 'we pay same day keywords' },
      { name: 'description', content: 'we pay same day description' },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'we pay same day' },
      { name: 'og:url', content: "https://test.wepaysameday.co.uk/" },
      { charset: 'UTF-8' }
    ]);
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  checkMessageDetailsPage(){
    var current_segment2 = window.location.pathname;
    return this.isMessageDetailsPage = current_segment2.match('message-details/');    
  }
  checkLoggedIn() {
    
    if (!window.localStorage.getItem('token')) {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }
    return this.loggedIn;
  }

}