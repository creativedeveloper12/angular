import { Component, OnInit,AfterViewInit , ElementRef, ViewChild } from '@angular/core';
import { ApiService } from "../service/api.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute } from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.scss']
})
export class MessageDetailsComponent implements OnInit {
  @ViewChild('scrollBottom') private scrollBottom: ElementRef;  
  loader = false;
  user_image:string;
  thread_id='';
  messageList: any;
  message='';
  error_message='';
  messageForm: FormGroup;
  searchForm: FormGroup;
  total= null;
  next= null;
  prev= null;
  next_url= null;
  prev_url= null;
  this_page= null;
  total_pages= null;

  page:number;
  search='';
  after='';

  limit=10;
  order='DESC';
  order_by='message_time';    
  array = [];

  constructor(private formBuilder: FormBuilder, private modalService: NgbModal, private router: Router,private _Activatedroute:ActivatedRoute,  private apiService: ApiService) { }

 
  onUp(next) {    
    if(next!=null){
      this.after=next;
      this.loader = true; 
      this.thread_id=this._Activatedroute.snapshot.paramMap.get("id");
      const params = this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.thread_id);
      this.apiService.getMessageList(params).subscribe(data => {
        //debugger;
        console.log(data);      
        if (data.status === true) {
          this.array=data.data;
          for (let i = data.data.length-1; i >=0; i--) {          
            this.messageList.unshift(data.data[i]);          
          }
          this.next=data.metadata.next;
          this.prev=data.metadata.prev;          

        } else {
          this.messageList =[];
          this.error_message=data.message;
        }     
        this.loader = false;
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
          this.messageList =[];
          this.loader = false;
        }
      });
    }
  }
  
  // AfterContentInit() {        
  //   this.scrollToBottom();        
  // } 
  // scrollToBottom(): void {    
  //     try {
  //         this.scrollBottom.nativeElement.scrollTop = this.scrollBottom.nativeElement.scrollHeight;
  //     } catch(err) { }
    
  // }
  
  ngOnInit(): void {
    this.loader = true;
    this.getMessageList();    
    this.messageForm = this.formBuilder.group({
      newMessage: ['', Validators.required],      
    });
    this.searchForm = this.formBuilder.group({
      searchText:this.search,      
    });
  }

  

  getRequestParams(search, page, limit, after, order_by, order, thread_id) {
    
    let params = {};
    if (search) {
      params[`search`] = search;
    }
    if (page) {
      params[`page`] = page;
    }
    if (limit) {
      params[`limit`] = limit;
    }
    if (after) {
      params[`after`] = after;
    }
    if (order_by) {
      params[`order_by`] = order_by;
    }
    if (order) {
      params[`order`] = order;
    }
    if (order) {
      params[`order`] = order;
    }
    if (thread_id) {
      params[`thread_id`] = thread_id;
    }
    return params;
  }

  getMessageList() { 
    this.loader = true; 
    this.thread_id=this._Activatedroute.snapshot.paramMap.get("id");
    const params = this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.thread_id);
    console.log(params);
   
    this.apiService.getMessageList(params).subscribe(data => {
      //debugger;
      console.log(data);      
      if (data.status === true) {
        this.messageList = data.data;
        this.next=data.metadata.next;
        this.prev=data.metadata.prev;
        if(this.messageList){
          this.messageList.reverse();
        }    
        setTimeout(()=>{ 
          this.scrollBottom.nativeElement.scrollTop = this.scrollBottom.nativeElement.scrollHeight;
          // this.scrollBottom.nativeElement.scroll({
          //   top: this.scrollBottom.nativeElement.scrollHeight,      
          //   left: 0,      
          //   behavior: 'smooth'      
          // });
        }, 20);     
      } else {
        this.messageList =[];
        this.error_message=data.message;
      }     
      this.loader = false;
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
        this.messageList =[];
        this.loader = false;
      }
    });
    this.autoHideMessage();
  }

  
  loadMessage(next){    
    let oldScroll= this.scrollBottom.nativeElement.scrollHeight;
    this.after=next;
    this.loader = true; 
    this.thread_id=this._Activatedroute.snapshot.paramMap.get("id");
    const params = this.getRequestParams(this.search, this.page, this.limit, this.after, this.order_by, this.order, this.thread_id);
    console.log(params);
   
    this.apiService.getMessageList(params).subscribe(data => {
      //debugger;
      console.log(data);      
      //console.log(this.messageList);      
      if (data.status === true) {       
        for (let i = 0; i < data.data.length; i++) {          
          this.messageList.unshift(data.data[i]);          
        }
        this.next=data.metadata.next;
        this.prev=data.metadata.prev;
        setTimeout(()=>{ 
          let newScroll= this.scrollBottom.nativeElement.scrollHeight;
          this.scrollBottom.nativeElement.scrollTop = newScroll-oldScroll;                       
        }, 10);  
      } else {
        this.messageList =[];
        this.error_message=data.message;
      }     
      this.loader = false;
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
        this.messageList =[];
        this.loader = false;
      }
    });
    this.autoHideMessage();
  }

  onChangeEvent(searchValue: any){ //when clear input text value
    if(searchValue==''){
      //this.page=1;
      this.after=null;
      this.search=searchValue;
      this.getMessageList();
    }
  }

  searchMessage() {
    //this.page=1;
    this.after=null;
    this.search=this.searchForm.controls.searchText.value;
    this.getMessageList();
  }

  onSubmit(){
    
    const messagePayload = {
      message: this.messageForm.controls.newMessage.value,  
      thread_id: this._Activatedroute.snapshot.paramMap.get("id"),  
    }
    this.apiService.sendMessage(messagePayload).subscribe(data => {
      //debugger;
     
      if(data.status === true) {        
        this.message=data.message; 
        this.after=null;      
        this.ngOnInit();
      }else {
        this.message=data.message;
        
      }
    },error => {     
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });
    this.autoHideMessage();
  }


  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }

  calculateTimeDiff(data){
    let date: any;
    let days :any;
    let currentDate: any;
    date = new Date(data);
    currentDate = new Date();
    // Math.floor((currentDate.getTime() - date.getTime()) / 1000);
    var d = Math.abs( currentDate- date) / 1000;   // delta
    
    var r = {year:0,week:0, month:0, day:0, hour:0, minute:0,second:0};                                                                // result
    var s = { year: 31536000,month: 2592000,week: 604800,day: 86400,hour: 3600,minute: 60,second: 1};
    
    Object.keys(s).forEach(function(key){
        r[key] = Math.floor(d / s[key]);
        d -= r[key] * s[key];
    });   
    
    if(r.year!=0){
      return days= r.year+' year ago';
    }
    if(r.month!=0){
      return days= r.month+' months ago';
    }

    if(r.week!=0){
      return days= r.week+' week ago';
    }

    if(r.day!=0){
      return days= r.day+' days ago';
    }

    if(r.hour!=0){
     return days= r.hour+' hours ago';
    }

    if(r.minute!=0){
      return days= r.minute+' minutes ago';
    }

    if(r.second!=0){
      return days= r.second+' second ago';
    }else{
      return days= " few seconds ago";
    }
    
  }

  getUserImage(){
    return this.user_image = localStorage.getItem('user_image');
  }

}
