import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router, ActivatedRoute} from "@angular/router";
import {ApiService} from "../service/api.service";

@Component({
  selector: 'app-register-fb',
  templateUrl: './register-fb.component.html',
  styleUrls: ['./register-fb.component.scss']
})
export class RegisterFBComponent implements OnInit {

  message='';
  error_message='';
  invalidLogin: boolean = false;
  registerForm: FormGroup;
  submitted = false;
  dob='';
  userEmail='';
  fName='';
  lName='';
  refer_code='';
  postcode='';
  token=''; 
  
  constructor(private formBuilder: FormBuilder, private router: Router, private _Activatedroute:ActivatedRoute, private apiService: ApiService) {
    if(this.router.getCurrentNavigation().extras.state != undefined){ 
      this.fName= this.router.getCurrentNavigation().extras.state.first_name;
      this.lName= this.router.getCurrentNavigation().extras.state.last_name;
      this.userEmail= this.router.getCurrentNavigation().extras.state.email;
      this.refer_code= this.router.getCurrentNavigation().extras.state.refer_code;
      this.postcode= this.router.getCurrentNavigation().extras.state.postcode;
      if(this.router.getCurrentNavigation().extras.state.token != undefined){
        this.token= this.router.getCurrentNavigation().extras.state.token;
      }
    }
  }

  ngOnInit(): void {
    //console.log(this._Activatedroute.snapshot.paramMap.get("referral"));
    this.registerForm = this.formBuilder.group({
      fName: ['', Validators.required],
      lName: ['', Validators.required],
      postcode: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      refer_code: [''],//this._Activatedroute.snapshot.paramMap.get("referral"),
      token: [''],
      
    });

    this.registerForm.get('fName').setValue(this.fName);
    this.registerForm.get('lName').setValue(this.lName);
    this.registerForm.get('email').setValue(this.userEmail);
    this.registerForm.get('postcode').setValue(this.postcode);
    this.registerForm.get('refer_code').setValue(this.refer_code);
    this.registerForm.get('token').setValue(this.token);
  }


  get registerFormControl() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {      
      // if (this.registerForm.invalid) {
      //   return;
      // }
      this.dob=this.registerForm.controls.year.value+'-'+this.registerForm.controls.month.value+'-'+this.registerForm.controls.day.value;
      const registerPayload = {
        firstname: this.registerForm.controls.fName.value,
        lastname: this.registerForm.controls.lName.value,
        email: this.registerForm.controls.email.value,
        postcode: this.registerForm.controls.postcode.value,
        dob: this.dob,
        refer_code: this.registerForm.controls.refer_code.value,
        step: '1', 
        //token:this.registerForm.controls.token.value,
      }
      //console.log(registerPayload); //return false;
      this.apiService.createUser(registerPayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          this.message=data.message;
          //alert(data.message);
          this.router.navigate(['emailverify'], { state: { email: this.registerForm.controls.email.value } });
          //, {queryParams: this.registerForm.controls.email.value}
        }else {
          this.error_message=data.message;
          alert(data.message);
        }
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });
    }
    this.autoHideMessage();
  }
  
  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }
  
  
}
