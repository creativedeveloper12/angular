import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFBComponent } from './register-fb.component';

describe('RegisterFBComponent', () => {
  let component: RegisterFBComponent;
  let fixture: ComponentFixture<RegisterFBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
