import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router, ActivatedRoute} from "@angular/router";
import {ApiService} from "../service/api.service";


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  message='';
  error_message='';
  userEmail='';
  invalidLogin: boolean = false;
  resetPasswordForm: FormGroup;  
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) {
    if(this.router.getCurrentNavigation().extras.state != undefined){  
      this.userEmail= this.router.getCurrentNavigation().extras.state.email;
      //this.message= this.router.getCurrentNavigation().extras.state.message;
    }
    
  }

  

  onSubmit() {
    if (this.resetPasswordForm.invalid) {
      return;
    }
    const passwordPayload = {
      email: this.resetPasswordForm.controls.email.value,
      code: this.resetPasswordForm.controls.emailCode.value,
      password: this.resetPasswordForm.controls.password.value,
      password_confirm: this.resetPasswordForm.controls.confirmpassword.value,
      step: '2'      
    }     
    this.apiService.forgotPassword(passwordPayload).subscribe(data => {
      //debugger;
      
      if(data.status === true) {
        //alert(this.message=data.message);
        this.message=data.message
        window.localStorage.setItem('token', data.data.token);
        window.localStorage.setItem('user_name', data.data.user_first_name+' '+data.data.user_last_name);
        this.router.navigate(['home']);
      }else {
        this.invalidLogin = true;
        this.error_message=data.message
        //alert(data.message);
      }
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });
    this.autoHideMessage();
  }


  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }
  ngOnInit(): void { 
    console.log(this.userEmail);
       
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],        
      confirmpassword: ['', Validators.required] ,       
      emailCode: ['', Validators.required],  
      name: ['']  
    },{ 
      validators: this.password.bind(this)
    });

    this.resetPasswordForm.get('email').setValue(this.userEmail); //set email value
    this.resetPasswordForm.get('name').setValue(this.userEmail);

  }


  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmpassword');
    if(confirmPassword!='' && password!=''){
      return password === confirmPassword ? null : { passwordNotMatch: true };
    }
    
  }

}
