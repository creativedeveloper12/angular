import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourReferralsComponent } from './your-referrals.component';

describe('YourReferralsComponent', () => {
  let component: YourReferralsComponent;
  let fixture: ComponentFixture<YourReferralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourReferralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourReferralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
