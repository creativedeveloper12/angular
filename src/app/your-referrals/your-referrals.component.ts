import { Component, OnInit } from '@angular/core';
import {ApiService} from "../service/api.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import { NgNavigatorShareService } from 'ng-navigator-share';

@Component({
  selector: 'app-your-referrals',
  templateUrl: './your-referrals.component.html',
  styleUrls: ['./your-referrals.component.scss']
})
export class YourReferralsComponent implements OnInit {
  loader = false;
  totalRewards='';
  referralList: any;
  copyMessage='';
  message='';
  error_message='';
  referralCode='';  
  referralForm: FormGroup;
  total= null;
  next= null;
  prev= null;
  next_url= null;
  prev_url= null;
  this_page= null;
  total_pages= null;
  page=1;
  limit=2;
  order='DESC';
  order_by='referral_updated_time';
  referralLink='';
  constructor(private formBuilder: FormBuilder,private apiService: ApiService, private ngNavigatorShareService: NgNavigatorShareService) { }

  ngOnInit(): void {
    
    this.getReferralList();
    this.apiService.getProfileDetails().subscribe(data => {
      //debugger;
      //console.log(data);
      if (data.status === true) {
        this.referralForm.get('referralCode').setValue(location.origin+'/register/'+data.data.user_refer_code);
        window.localStorage.setItem('user_image', data.data.user_profile_image);
        this.referralLink=location.origin+'/register/'+data.data.user_refer_code;
      } else {
        this.message=data.message;
        //alert(data.message);
      }
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    }); 
    this.referralForm = this.formBuilder.group({
      referralCode: this.referralCode,
    });
  }


  getRequestParams(page, limit,  order_by, order) {
    // tslint:disable-next-line:prefer-const
    let params = {};
    
    if (page) {
      params[`page`] = page;
    }
    if (limit) {
      params[`limit`] = limit;
    }    
    if (order_by) {
      params[`order_by`] = order_by;
    }
    if (order) {
      params[`order`] = order;
    }

    return params;
  }

  getReferralList() {  
    this.loader = true;  
    const params = this.getRequestParams(this.page, this.limit,this.order_by, this.order);
    console.log(params);
    this.apiService.getReferralList(params).subscribe(data => {
      if (data.status === true) {
        if( data.data!==null){        
          this.referralList = data.data;
          
        }else{
          this.referralList =[];
         
        }
        this.total_pages = data.metadata.total_pages;
        this.this_page= data.metadata.this_page;
        this.total= data.metadata.total;
        this.next= data.metadata.next;
        this.prev= data.metadata.prev;
        this.next_url= data.metadata.next_url;
        this.prev_url= data.metadata.prev_url;
        this.limit= data.metadata.limit;
        this.totalRewards=data.metadata.total_rewards;
        console.log(data);
        this.loader = false;
      } else {
        this.referralList = [];
        this.loader = false;
      }
    },
    error => {
      console.log(error);
      this.loader = false;
    });
  }


  copyInputMessage(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.copyMessage="url copied";
    this.autoHideMessage();
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.copyMessage='';
    }, 5000);
  }



  share(link) {    
    if (!this.ngNavigatorShareService.canShare()) {
      alert(`This service/api is not supported in your Browser`);
      return;
    } 
    this.ngNavigatorShareService.share({
      title: 'We Pay Same Day',
      text: 'Hi there, Join me on the We Pay Same Day and get Rewards',
      url: link
    }).then( (response) => {
      console.log(response);
    })
    .catch( (error) => {
      console.log(error);
    });
  }

  shareFB(link) { 
    var facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(link),
      'facebook-popup',
      'height=350,width=600'
    );
    if (facebookWindow.focus) {
      facebookWindow.focus();
    }
    return false;
  }

}
