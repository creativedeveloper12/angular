import { Component, HostListener, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router, ActivatedRoute} from "@angular/router";
import {ApiService} from "../service/api.service";
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider } from "angularx-social-login";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})


export class RegisterComponent implements OnInit {
  deferredPrompt: any;
  showButton = false;
  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(e: { preventDefault: () => void; }) {
    console.log(e);
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    this.deferredPrompt = e;
    this.showButton = true;
  }

  message='';
  error_message='';
  invalidLogin: boolean = false;
  registerForm: FormGroup;
  submitted = false;
  dob='';
  constructor(private formBuilder: FormBuilder, private router: Router, private _Activatedroute:ActivatedRoute, private apiService: ApiService, private authService: SocialAuthService) { }

  
  addToHomeScreen() {
    // hide our user interface that shows our A2HS button
    this.showButton = false;
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice
      .then((choiceResult: { outcome: string; }) => {
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the A2HS prompt');
        } else {
          console.log('User dismissed the A2HS prompt');
        }
        this.deferredPrompt = null;
      });
  }

  isAppInstall(){
    return this.showButton;
  }

  
  get registerFormControl() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {      
      // if (this.registerForm.invalid) {
      //   return;
      // }
      this.dob=this.registerForm.controls.year.value+'-'+this.registerForm.controls.month.value+'-'+this.registerForm.controls.day.value;
      const registerPayload = {
        firstname: this.registerForm.controls.fName.value,
        lastname: this.registerForm.controls.lName.value,
        email: this.registerForm.controls.email.value,
        password: this.registerForm.controls.password.value,
        postcode: this.registerForm.controls.postcode.value,
        dob: this.dob,
        refer_code: this.registerForm.controls.refer_code.value,
        step: '1', 
      }
      //console.log(registerPayload); return false;
      this.apiService.createUser(registerPayload).subscribe(data => {
        //debugger;
        console.log(data);
        if(data.status === true) {
          this.message=data.message;
          //alert(data.message);
          this.router.navigate(['emailverify'], { state: { email: this.registerForm.controls.email.value } });
          //, {queryParams: this.registerForm.controls.email.value}
        }else {
          this.error_message=data.message;
          alert(data.message);
        }
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });
    }
    this.autoHideMessage();
  }
  
  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }
  
  ngOnInit(): void {    
    this.registerForm = this.formBuilder.group({
      fName: ['', Validators.required],
      lName: ['', Validators.required],
      password: ['', Validators.required],
      //mobile: ['', Validators.required],
      postcode: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      refer_code: this._Activatedroute.snapshot.paramMap.get("referral"),

    });

    
  }

  getYears() {
    const yearsList = [];
    const startYear=1920;
    const currentYear = new Date().getFullYear();
    for (let index = 0; index <= currentYear - startYear; index++) {
      yearsList.push(startYear + index);
    }
    return {yearsList};
  }

  signInWithFB(): void {

    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID, {cookie:true, xfbml:true, version:'v11.0'})
    .then(res => { 
      //console.log(res);      
      if(res.authToken!=''){
        const loginPayload = {
          token: res.authToken,        
        }
        //console.log(loginPayload);
        this.apiService.facebookLogin(loginPayload).subscribe(data => {        
          //console.log(data);           
          if(data.status === true) {
            if(data.metadata.twofactorrequired===false && data.metadata.registrationrequired===false){
              window.localStorage.setItem('token', data.data.token);
              this.router.navigate(['home']);
              this.message=data.message;
              this.apiService.getProfileDetails().subscribe(userData => {
                //debugger;
                if (userData.status === true) {
                  
                  window.localStorage.setItem('token', window.localStorage.getItem('token'));
                  window.localStorage.setItem('user_name', userData.data.user_first_name+' '+userData.data.user_last_name);     
                } else {
                  this.message=userData.message;            
                }
              });
            }else if(data.metadata.twofactorrequired===true && data.metadata.registrationrequired===false){
              this.message=data.message;
              this.router.navigate(['emailverify'], { state: { email: data.data.email, twofactortoken:data.data.twofactortoken, token:res.authToken } });
            }else if(data.metadata.twofactorrequired===false && data.metadata.registrationrequired===true){
              
              this.router.navigate(['register-fb'], { state: { token:res.authToken, first_name: data.data.registration_suggestions.first_name, last_name: data.data.registration_suggestions.last_name, email: data.data.registration_suggestions.email, refer_code: data.data.registration_suggestions.refer_code, postcode: data.data.registration_suggestions.postcode } });
            }           
    
          }else {
            this.invalidLogin = true;
            this.message=data.message;
          }
        },error => {      
          if(error.status==401){
            this.message=error.error.message;
            console.log(error);
          }
        });
      }      
    });
  }



}
