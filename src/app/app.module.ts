import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { ListUserComponent } from './user/list-user/list-user.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ApiService } from "./service/api.service";
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from "@angular/common/http";
import { TokenInterceptor } from "./core/interceptor";
import { MessagesComponent } from './messages/messages.component';
import { SettingsComponent } from './settings/settings.component';
import { FavouriteJobsComponent } from './favourite-jobs/favourite-jobs.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { JobOfferComponent } from './job-offer/job-offer.component';
import { YourReferralsComponent } from './your-referrals/your-referrals.component';
import { YourJobsComponent } from './your-jobs/your-jobs.component';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RegisterFBComponent } from './register-fb/register-fb.component';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ListUserComponent,
    LoginComponent,
    RegisterComponent,
    EmailVerifyComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    AddUserComponent,
    EditUserComponent,
    HomeComponent,
    ProfileComponent,
    MessagesComponent,
    SettingsComponent,
    FavouriteJobsComponent,
    MessageDetailsComponent,
    JobOfferComponent,
    YourReferralsComponent,
    YourJobsComponent,
    CalendarViewComponent,
    RegisterFBComponent,



  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgxPaginationModule,
    InfiniteScrollModule,
    AutocompleteLibModule,
    SocialLoginModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }) 
  ],
  providers: [ApiService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,   
  },
  {
    provide:'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        // {
        //   id: GoogleLoginProvider.PROVIDER_ID,
        //   provider: new GoogleLoginProvider(
        //     'clientId'
        //   )
        // },
        {
          id: FacebookLoginProvider.PROVIDER_ID,          
          provider: new FacebookLoginProvider('1248209358971535') //same
        }
      ]
    } as SocialAuthServiceConfig,
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
