import { Component, OnInit} from '@angular/core';
import { ApiService } from "../service/api.service";
import { NgbModal, ModalDismissReasons, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss']
})
export class CalendarViewComponent implements OnInit {
  loader = false;
  isAgree: boolean = false;
  isMobile: boolean = false;
  closeResult: string;
  calendarList:any;
  this_month='';
  this_month_name='';
  next_link='';
  prev_link='';
  this_year='';
  order='DESC';
  order_by='job_id';
  radius=100;
  currentMonth:any;
  jobs:any;
  jobDetails: any;
  currentJobDate:any;
  message_apply='';
  error_message_apply='';
  message='';
  error_message='';
  selected_date='';
  constructor(private modalService: NgbModal, private apiService: ApiService) { }

  ngOnInit(): void {
    let todayDate= new Date();
    let currentDate = formatDate(todayDate, 'yyyy-MM-dd', 'en');
    this.getCalendarView(currentDate);
    this.currentMonth= todayDate.getMonth()+1;
    //console.log(this.currentMonth);
    this.getJobList(currentDate);
    this.currentJobDate=currentDate;

    if(/Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      this.isMobile= true;      
    }else{
      // false for not mobile device
      this.isMobile= false;
    }

  }

  getJobList(date) {
    this.selected_date =date;
    this.jobs =[];
    this.currentJobDate=date;
    this.loader = true;
    const JobPayload = {
      date: date, 
      order:this.order,     
      order_by:this.order_by,     
      radius:this.radius,     
    } 
    this.apiService.getJobs(JobPayload).subscribe(data => {
      console.log(data);
      if (data.status === true) {
        if( data.data!==null){        
          this.jobs = data.data;                          

        }else{
          this.jobs =[];          
        }      
        
      } else {
        this.jobs = [];        
      }
      this.loader = false;
      if(this.isMobile== true){            
        window.scroll({ 
          top: 0, 
          left: 0, 
          behavior: 'smooth' 
        });
      }
      console.log(this.isMobile); 
    },
    error => {
      console.log(error);
      this.loader = false;      
    });
    
  }

  getCalendarView(dateMonth){
    this.loader = true;
    this.jobs =[];
    this.currentMonth=new Date(dateMonth).getMonth()+1;
    this.currentJobDate=dateMonth;
    const datePayload = {
      start_date: dateMonth,
    }
    this.apiService.getCalendarView(datePayload).subscribe(data => {
      //debugger;
      console.log(data);      
      if (data.status === true) {
        this.calendarList = data.data;
        this.this_month=data.metadata.this_month;
        this.this_month_name=data.metadata.this_month_f;
        this.next_link=data.metadata.next_link;
        this.prev_link=data.metadata.prev_link;
        this.this_year=data.metadata.this_year;  
      } else {
        this.calendarList =[];
      }     
      this.loader = false;
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
        this.calendarList =[];
        this.loader = false;
      }
    });
  }

  getMonthCalendar(thisMonth){
    this.getCalendarView(thisMonth);
  }

  checkCurrentMonth(date){
    //console.log(date);
    var today = new Date(date);
    var month = today.getMonth()+1; // Returns 9
    return month;
  }


  open(content, id) { 
    this.isAgree= false;  
    this.message_apply='';     
    this.error_message_apply='';     
    this.apiService.viewJob(id).subscribe(data => {
      //debugger;     
      console.log(data.data); 
      if (data.status === true) {
        this.jobDetails = data.data;        
      } else {
        this.jobDetails = [];
      }

      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });

    });
        
  }

  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }


  favouriteJobAddDetails(id) {  
    const favJobPayload = {
      job_id: id,      
    }    
    this.apiService.favouriteJobAdd(favJobPayload).subscribe(data => {
      //debugger;
      console.log(data);
      
      if (data.status === true) {
        //alert(data.message);
        this.apiService.viewJob(id).subscribe(res => {
          //debugger;     
          console.log(res.data); 
          if (res.status === true) {
            this.jobDetails = res.data;        
          }           
        });
      } else {
        //alert(data.message);
        this.error_message=data.message;

      }      
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });

    this.autoHideMessage(); 
          
  }


  calculateDiff(data){
    let date = new Date(data);
    let currentDate = new Date();

    let days = Math.floor((currentDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24);
    return days;
  }


  toggleShow() {
    this.isAgree = ! this.isAgree;
  }

  applyJob(id) {  
    const jobPayload = {
      job_id: id,      
    }      
    this.apiService.applyJob(jobPayload).subscribe(data => {
      //debugger;
      console.log(data);
      
      if (data.status === true) {
        this.message_apply=data.message;

        //alert(data.message);
      } else {
        this.error_message_apply=data.message;
        //alert(data.message);
      }
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message_apply=error.error.message;
      }
    });
    setTimeout(()=>{ 
      this.message_apply='';                      
      this.error_message_apply='';
    }, 5000);   
  }


  offerJobEdit(id, action) {  
    const JobOfferPayload = {
      job_id: id, 
      action:action     
    }
    console.log(JobOfferPayload);      
    this.apiService.offerJobEdit(JobOfferPayload).subscribe(data => {
      //debugger;
      console.log(data);
      if (data.status === true) {
        //alert(data.message);
        this.message=data.message;        
        this.apiService.viewJob(id).subscribe(data => {
          //debugger;     
          if (data.status === true) {
            this.jobDetails = data.data;        
          } else {
            this.jobDetails = [];
          }    
          this.getJobList(this.selected_date);
        });
      } else {
        //alert(data.message);
        this.error_message=data.message;
      }
      // this.ngOnInit();
    },
    error => {      
      if(error.status==400){
        console.log(error.error.message);
        this.error_message=error.error.message;
      }
    });  
    this.autoHideMessage();     
  }

}
