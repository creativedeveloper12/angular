import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "../service/api.service";


@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.scss']
})
export class EmailVerifyComponent implements OnInit {
  message='';
  error_message='';
  userEmail = ''; 
  twofactortoken=''; 
  token=''; 
  invalidLogin: boolean = false;
  emailVerifyForm: FormGroup;  
  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) {
    if(this.router.getCurrentNavigation().extras.state != undefined){ 
      this.userEmail= this.router.getCurrentNavigation().extras.state.email;
      if(this.router.getCurrentNavigation().extras.state.twofactortoken != undefined){
        this.twofactortoken= this.router.getCurrentNavigation().extras.state.twofactortoken;
      } 
      if(this.router.getCurrentNavigation().extras.state.token != undefined){
        this.token= this.router.getCurrentNavigation().extras.state.token;
      } 
      console.log(this.token);     
    }
   }

   moveOnMax(event: any, nextFieldID): void {    
    // console.log(event); 
    // console.log(event.target.value.length); 
    if (event.target.value.length >= 1 && nextFieldID!='') {
      // if(event.keyCode!=16 || event.keyCode!=9 ){
        document.getElementById('v'+nextFieldID).focus();
      // }
      
    }
    // if (event.keyCode === 8) {          
    //   let nextFieldIDP=nextFieldID-2;
    //   if(nextFieldIDP>0){
    //     document.getElementById('v'+nextFieldIDP).focus();
    //   }         
    // }   
}
  
  onSubmit() {
    if (this.emailVerifyForm.invalid) {
      return;
    }
    
    
    if(this.emailVerifyForm.controls.twofactortoken.value==''){
      const emailPayload = {
        email: this.emailVerifyForm.controls.email.value,
        step: '2',
        code: this.emailVerifyForm.controls.v1.value+this.emailVerifyForm.controls.v2.value+this.emailVerifyForm.controls.v3.value+this.emailVerifyForm.controls.v4.value+this.emailVerifyForm.controls.v5.value+this.emailVerifyForm.controls.v6.value,        
      }
      //console.log(emailPayload);return false;
      this.apiService.createUser(emailPayload).subscribe(data => {
        //debugger;
        console.log(data);
        console.log(data.data.user_first_name);
        if(data.status === true) {        
          window.localStorage.setItem('token', data.data.token);
          window.localStorage.setItem('user_name', data.data.user_first_name+' '+data.data.user_last_name);
          this.message=data.message;
          //alert(data.message);
          this.router.navigate(['home']);
        }else {
          this.invalidLogin = true;
          //alert(data.message);
          this.error_message=data.message
        }
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });

    }else{
      const emailPayload = {
        email: this.emailVerifyForm.controls.email.value,
        step: '2',
        code: this.emailVerifyForm.controls.v1.value+this.emailVerifyForm.controls.v2.value+this.emailVerifyForm.controls.v3.value+this.emailVerifyForm.controls.v4.value+this.emailVerifyForm.controls.v5.value+this.emailVerifyForm.controls.v6.value,
        '2factortoken':this.emailVerifyForm.controls.twofactortoken.value,
        token:this.emailVerifyForm.controls.token.value,
      }
      //console.log(emailPayload);return false;
      this.apiService.facebookLogin(emailPayload).subscribe(userData => {
        //debugger;
        if(userData.status === true) {        
          window.localStorage.setItem('token', userData.data.token);
          window.localStorage.setItem('user_name', userData.data.first_name+' '+userData.data.last_name);
          this.message=userData.message;
          this.router.navigate(['home']);
        }else {
          this.invalidLogin = true;
          //alert(data.message);
          this.error_message=userData.message
        }
      },
      error => {      
        if(error.status==400){
          console.log(error.error.message);
          this.error_message=error.error.message;
        }
      });
    }

    this.autoHideMessage();
  }


  ngOnInit(): void {

    this.emailVerifyForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      twofactortoken: [''],
      token: [''],
      v1: ['', Validators.required],
      v2: ['', Validators.required],
      v3: ['', Validators.required],
      v4: ['', Validators.required],
      v5: ['', Validators.required],
      v6: ['', Validators.required]  
    });
    this.emailVerifyForm.get('email').setValue(this.userEmail); //set email value
    this.emailVerifyForm.get('twofactortoken').setValue(this.twofactortoken);
    this.emailVerifyForm.get('token').setValue(this.token);
  }



  autoHideMessage(){
    setTimeout(()=>{ 
      this.message='';                      
      this.error_message='';
    }, 5000);
  }


}
